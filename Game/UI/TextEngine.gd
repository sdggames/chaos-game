extends CanvasLayer

export var player_text_color: Color
export var anxiety_lvl1_text_color: Color
export var anxiety_lvl2_text_color: Color
export var anxiety_lvl3_text_color: Color
export var doctor_text_color: Color
export var stranger_text_color: Color

# The two text boxes
onready var top_text := $ColorRect/VBoxContainer/Top
onready var bottom_text := $ColorRect/VBoxContainer/Bottom

var active_script := []
var next_line := 0
var reading_paused := true

signal reading_paused
signal monster_enter
signal anxiety_level( level )
signal narrative_event( event )

# Story events
enum {
	NONE,
	CLEAR,
	PAUSE,
	HIDE,
	
	# Emit special signals
	SIGNAL

	# Anxiety events, monster is coming.
	ANXIETY_LVL1,
	ANXIETY_LVL2,
	ANXIETY_LVL3,
	MONSTER_ENTR,
	CLEAR_ANXIETY,
	
	# Other text colors, different speakers.
	DOCTOR_SPEAK,
	STRANGER_SPEAK,
}

var story_script = [
	[
		[
			[1, CLEAR, "There is no Chapter 0."]
		]
	],
	# Chapter 1
	[
		# Scene zero...
		[],
		# Scene 1
		[
			[3, NONE, "Oh my goodness! What is that?"],
			[3, NONE, "That doesn't look good!"],
			[4, CLEAR, ""],
			[3, NONE, "Wow, life can really end just like that."],
			[3, NONE, "Those poor people!"],
			[4, CLEAR, ""],
			[3, ANXIETY_LVL1, "What if that was me?"],
			[3, ANXIETY_LVL1, "My life could have ended today."],
			[3, ANXIETY_LVL2, "Would anybody miss me?"],
			[2, ANXIETY_LVL3, "Does my life even matter?"],
			[0, MONSTER_ENTR, ""],
			[0, HIDE, ""],
		],
		# Scene 2
		[
			[4, DOCTOR_SPEAK, "[Doctor] Look, this is hard for everyone."],
			[4, DOCTOR_SPEAK, "We all wish that we lived in a utopia."],
			[4, DOCTOR_SPEAK, "Everyone wants to be important. To matter."],
			[5, DOCTOR_SPEAK, "But..."],
			[0, DOCTOR_SPEAK, "*sigh*"],
			[5, DOCTOR_SPEAK, "We're all in this together. Try to focus on the positive."],
			[5, DOCTOR_SPEAK, "Please at least consider the counseling option."],
			[5, DOCTOR_SPEAK, "This year has been hard for us all."],
			[4, CLEAR, ""],
			[5, DOCTOR_SPEAK, "Stay safe out there, and call me if you need anything."],
			[0, SIGNAL, "", "Chapter_End"]
		],
	],
	# Chapter 2
	[
		# Scene zero...
		[],
		# Scene 1
		[
			[3, NONE, "What even is the point?"],
			[3, SIGNAL, "We live, we die. Then what?", "Shopping"],
			[3, CLEAR, ""],
			[3, NONE, "Birds don't have anxiety."],
			[4, NONE, "Most dogs are happy, as long as they're fed."],
			[3, NONE, "So what makes me so different?"],
			[3, ANXIETY_LVL1, "Why do I feel like I don't belong?"],
			[4, CLEAR, ""],
			[2, ANXIETY_LVL1, "Uh-oh, I think it's coming back. I better be quick!"],
			[2, ANXIETY_LVL1, "Think happy thoughts..."],
			[3, CLEAR, ""],
			[5, ANXIETY_LVL2, "When was the last time I was truly happy?"],
			[0, MONSTER_ENTR, "I... I don't know."],
			[5, SIGNAL, "", "Freeze"],
			[5, NONE, "I have to say sane. I have to. I have to..."],
			[0, SIGNAL, "", "Crash"],
		],
		# Scene 2
		[
			[1, CLEAR, ""]
		],
	],
	# Chapter 3
	[
		# Scene zero...
		[],
		# Scene 1
		[
			[4, STRANGER_SPEAK, "[Stranger] BE GONE!"],
			[1, CLEAR, ""],
			[4, NONE, "You can see it too?"],
			[1, CLEAR, ""],
			[3, STRANGER_SPEAK, "No, but I can feel the darkness that you carry."],
			[3, STRANGER_SPEAK, "I was the same once."],
			[3, STRANGER_SPEAK, "Why don't you sit down?"],
			[0, SIGNAL, "", "Sit"],
			[1, CLEAR, ""],
			[0, HIDE, ""],
			[4, STRANGER_SPEAK, "MY STORY HERE"],
			[0, SIGNAL, "", "Combat"],
			[0, HIDE, ""],
			
			[0, CLEAR, ""],
			[0, NONE, "I Matter!"],
			[0, PAUSE, ""],
			[0, CLEAR, ""],
			[0, NONE, "I Am Loved!"],
			[0, PAUSE, ""],
			[0, CLEAR, ""],
			[0, NONE, "I Have A Purpose!"],
			[0, PAUSE, ""],
			[0, CLEAR, ""],
			[0, NONE, "Evil Has Not Won!"],
			[0, PAUSE, ""],
			[0, CLEAR, ""],
			[0, NONE, "I WILL NOT BE FRIGHTENED BY LIES!"],
			[0, SIGNAL, "", "MonsterSlain"],
			[0, PAUSE, ""],
		],
	],
]


func load_scene(chapter, scene):
	active_script = story_script[chapter][scene]
	next_line = 0


func start_scene(chapter, scene):
	load_scene(chapter, scene)
	start_reading()


func start_reading():
	if reading_paused:
		reading_paused = false
		_read()


func _read():
	$ColorRect.visible = true
	if !reading_paused:
		# Check if we finished this script. If so, don't try to read.
		if next_line >= active_script.size():
			reading_paused = true
		else:
			var read_time = _read_line(active_script[next_line])
			next_line += 1
			Global.wait(read_time)


func _ready():
	active_script = story_script[0][0]
	var _d = Global.connect("reading_timer", self, "_read")
	
	Console.add_command("read_story", self, 'load_scene')\
	.set_description("Read the story for the selected chapter and scene." +\
		 "Not zero-indexed. Example use: read_story 1 1")\
	.add_argument('chapter', TYPE_INT)\
	.add_argument('scene', TYPE_INT)\
	.register()
	
	Console.add_command("start_reading", self, 'start_reading')\
	.set_description("Continue reading where we left off.")\
	.register()


func _read_line(line: Array) -> float:
	var should_read = _text_event(line[1], line)
	if should_read:
		if top_text.text == "" and bottom_text.text == "":
			top_text.text = line[2]
		else:
			if bottom_text.text != "":
				top_text.text = bottom_text.text
			bottom_text.text = line[2]
	return line[0]


# Process the special event flag on this line of dialogue
func _text_event(event, line) -> bool:
	var should_read := true
	match event:
		NONE:
			_set_color(player_text_color)
		CLEAR:
			$ColorRect.visible = false
			_set_color(player_text_color)
			_set_color(player_text_color)
			bottom_text.text = ""
			top_text.text = ""
			should_read = false
		PAUSE:
			emit_signal("reading_paused")
			reading_paused = true
			should_read = false
		HIDE:
			emit_signal("reading_paused")
			reading_paused = true
			should_read = false
			$ColorRect.visible = false
		
		# Emit a catch-all signal that can be used for narrative hooks.
		# Dialogue line MUST have one extra parameter to pass on.
		SIGNAL:
			_set_color(player_text_color)
			emit_signal("narrative_event", line[3])
		
		# Anxiety events, monster is coming.
		ANXIETY_LVL1:
			_set_color(anxiety_lvl1_text_color)
			emit_signal("anxiety_level", 1)
			$Lvl1.visible = true
		ANXIETY_LVL2:
			_set_color(anxiety_lvl2_text_color)
			emit_signal("anxiety_level", 2)
			$Lvl2.visible = true
		ANXIETY_LVL3:
			_set_color(anxiety_lvl3_text_color)
			emit_signal("anxiety_level", 3)
			$Lvl3.visible = true
		MONSTER_ENTR:
			_set_color(anxiety_lvl3_text_color)
			emit_signal("monster_enter")
			$Lvl3.visible = true
		CLEAR_ANXIETY:
			$Lvl1.visible = false
			$Lvl2.visible = false
			$Lvl3.visible = false
		
		# Other text colors, different speakers.
		DOCTOR_SPEAK:
			_set_color(doctor_text_color)
		STRANGER_SPEAK:
			_set_color(stranger_text_color)

	return should_read


func _set_color(color: Color):
	if top_text.text == "" and bottom_text.text == "":
		top_text.set("custom_colors/font_color", color)
	else:
		if bottom_text.text != "":
			top_text.set("custom_colors/font_color", bottom_text.get("custom_colors/font_color"))
		bottom_text.set("custom_colors/font_color", color)


func _exit_tree():
	Console.remove_command("read_story")
	Console.remove_command("start_reading")
