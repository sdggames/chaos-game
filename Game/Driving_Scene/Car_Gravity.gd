extends RigidBody2D

export var driving_with_player := false
export var drive_speed := 200
var drive_movement := Vector2.ZERO

func _ready():
	if driving_with_player:
		$Sprite.animation = "Right"
		$Sprite.frame = randi() % 3
		drive_movement = Vector2.RIGHT * drive_speed
	else:
		$Sprite.animation = "Left"
		$Sprite.frame = randi() % 3
		drive_movement = Vector2.LEFT * drive_speed


func _on_TooClose_body_entered(body):
	if body.is_in_group("Player") and !$Honk_Repeated.playing and !$Honk_Solid.playing:
		$Honk_Solid.play()


func _on_AreaL_body_entered(body):
	if body.is_in_group("Player") and !driving_with_player and \
			!$Honk_Repeated.playing and !$Honk_Solid.playing:
		$Honk_Repeated.play()


func _on_AreaR_body_entered(body):
	if body.is_in_group("Player") and driving_with_player and \
			!$Honk_Repeated.playing and !$Honk_Solid.playing:
		$Honk_Repeated.play()
