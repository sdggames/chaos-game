extends Node2D

export var scroll_speed = 210
export var scroll_increment = 50

signal scene_end
var scene_ending = false


func _ready():
	Global.reset_score()
	Global.scroll_speed = scroll_speed


func _on_Player_hit_car():
	if !scene_ending:
		scene_ending = true
		yield(get_tree().create_timer(0.5), "timeout")
		emit_signal("scene_end")


func _player_out_of_bounds(_body):
	_on_Player_hit_car()


func _on_timeout():
	scene_ending = true
	get_parent()._next_chapter_silent()


func _faster():
	Global.scroll_speed += scroll_increment
	Global.score_factor *= 2
