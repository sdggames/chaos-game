extends Area2D

export var spawn_chance_per_frame := 100.0
export(Resource) var cone_prefab


var cones = []


func _process(_delta):
	if get_overlapping_areas().size() == 0 and get_overlapping_bodies().size() == 0:
		if rand_range(0, 100) < spawn_chance_per_frame:
			create_cone()


func create_cone():
	if cones.size() > 0:
		var existing_cone = cones[-1]
		if existing_cone.global_transform.origin.x < -100:
			existing_cone.global_transform.origin = position
			existing_cone.global_rotation_degrees = 0
			cones.remove(cones.size() - 1)
			cones.append(existing_cone)
			existing_cone._ready()
		else:
			var cone = cone_prefab.instance()
			add_child(cone)
			cones.append(cone)
	else:
		var cone = cone_prefab.instance()
		add_child(cone)
		cones.append(cone)

