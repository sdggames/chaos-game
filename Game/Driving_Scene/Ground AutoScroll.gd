extends Node2D

export var repeat_texture_width := 768

func _physics_process(delta):
	transform.origin -= Vector2(Global.scroll_speed * delta, 0)
	if transform.origin.x < -repeat_texture_width:
		transform.origin += Vector2(repeat_texture_width, 0)
