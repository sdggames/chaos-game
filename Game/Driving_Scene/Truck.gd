extends Sprite

signal pancake

var boost_armed := false
var timer_armed := false
var player_in_killbox := false
var warning := false
var moving := false

func arm():
	boost_armed = true


func _on_Timer_timeout():
	if player_in_killbox:
		timer_armed = true


func _on_Killbox_body_entered(body):
	if body.is_in_group("Player"):
		$Timer.start()
		player_in_killbox = true


func _on_Killbox_body_exited(body):
	if body.is_in_group("Player"):
		$Timer.stop()
		player_in_killbox = false


func _process(delta):
	if boost_armed and timer_armed and player_in_killbox and !warning:
		warning = true
		# Only bad things can happen from this point on.
		$KillTimer.start()
		$AudioStreamPlayer.play()
	if moving:
		# It's hard to stop a train... or whatever
		position.x -= Global.scroll_speed * 1.5 * delta
		
		if position.x < -1000:
			position.x = 5000


func _on_Pancake_body_entered(body):
	if body.is_in_group("Player"):
		body.position.x = -10000
		$AudioStreamPlayer2.play()
		emit_signal("pancake")


func _on_KillTimer_timeout():
	$Pancake.monitoring = true
	moving = true
