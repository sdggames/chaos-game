extends Node2D

export var scroll_speed = 300
export var scroll_increment = 50
export var easter_egg = false

signal scene_end

onready var score = $ColorRect/Label
onready var high_score = $ColorRect2/Label


func _ready():
	Global.scene_number_is_high_enough_for_us_to_do_the_flashy_thing = true
	Global.reset_score()
	Global.scroll_speed = scroll_speed


func _process(_delta):
	score.text = "Score: " + str(Global.score)
	high_score.text = "High Score: " + str(Global.high_score)


func _physics_process(_delta):
	Global.lower_score(scroll_speed)


func _on_Player_hit_car():
	if !Global.scene_ending:
		Global.scene_ending = true
		if Global.high_score_is_high:
			$GameOver.visible = true
			$AudioStreamPlayer.stop()
			get_parent().play_scene_end_sound()
		else:
			yield(get_tree().create_timer(1.5), "timeout")
			emit_signal("scene_end")


func _player_out_of_bounds(_body):
	if easter_egg and !Global.cheat_bug_found and !Global.scene_ending:
		Global.cheat_bug_found = true
		get_parent()._select_scene("why2")
	else:
		_on_Player_hit_car()


func _faster():
	Global.scroll_speed += scroll_increment
	Global.score_factor += 3


func _on_Player_hit_cone():
	_faster() # Faster!


func _on_NewGame_pressed():
	get_parent()._select_scene_silent("1-6r")
