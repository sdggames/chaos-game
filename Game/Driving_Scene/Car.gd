extends KinematicBody2D

export var driving_with_player := false
export var drive_speed := 200
var drive_movement := Vector2.ZERO
var party_mode := false


func _ready():
	if driving_with_player:
		$Sprite.animation = "Right"
		$Sprite.frame = randi() % 3
		drive_movement = Vector2.RIGHT * drive_speed
	else:
		$Sprite.animation = "Left"
		$Sprite.frame = randi() % 3
		drive_movement = Vector2.LEFT * drive_speed


func _process(_delta):
	if !party_mode and Global.high_score_is_high:
		party_mode = true
		$Sprite.playing = true


func _physics_process(_delta):
	if is_visible_in_tree():
		var world_movement = Vector2.LEFT * Global.scroll_speed
		var _d = move_and_slide(world_movement + drive_movement)
		if transform.origin.x < -184:
			transform.origin = Vector2(5000, transform.origin.y)
			Global.add_score()


func _on_TooClose_body_entered(body):
	if body.is_in_group("Player") and !$Honk_Repeated.playing and !$Honk_Solid.playing:
		$Honk_Solid.play()


func _on_AreaL_body_entered(body):
	if body.is_in_group("Player") and !driving_with_player and \
			!$Honk_Repeated.playing and !$Honk_Solid.playing:
		$Honk_Repeated.play()


func _on_AreaR_body_entered(body):
	if body.is_in_group("Player") and driving_with_player and \
			!$Honk_Repeated.playing and !$Honk_Solid.playing:
		$Honk_Repeated.play()
