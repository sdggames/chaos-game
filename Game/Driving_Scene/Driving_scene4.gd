extends Node2D

export var scroll_speed = 300
export var scroll_increment = 50
export var easter_egg = false

signal scene_end

onready var score = $ColorRect/Label
onready var high_score = $ColorRect2/Label


func _ready():
	Global.reset_score()
	Global.scroll_speed = scroll_speed


func _process(_delta):
	score.text = "Score: " + str(Global.add_score())
	high_score.text = "High Score: " + str(float(Global.high_score))


func _on_Player_hit_car():
	if !Global.scene_ending:
		Global.scene_ending = true
		yield(get_tree().create_timer(0.5), "timeout")
		emit_signal("scene_end")


func _player_out_of_bounds(_body):
	if easter_egg and !Global.cheat_bug_found:
		Global.cheat_bug_found = true
		get_parent()._select_scene("why2")
	else:
		_on_Player_hit_car()


func _faster():
	Global.scroll_speed += scroll_increment
	Global.score_factor += 1
	if Global.score_factor > 150 and !Global.scene_ending:
		Global.scene_ending = true
		$Player.back_to_the_fuuuuuture()
		yield(get_tree().create_timer(1), "timeout")
		get_parent()._select_scene_silent("zoom")


func _on_Player_hit_cone():
	_faster() # Faster!
