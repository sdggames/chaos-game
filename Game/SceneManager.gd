extends Control

export(Resource) var menu_scene
export(Resource) var driving_scene
export(Resource) var driving_scene2
export(Resource) var driving_scene3
export(Resource) var driving_scene4
export(Resource) var driving_scene5
export(Resource) var driving_scene6
export(Resource) var doctor_scene
export(Resource) var shopping_scene
export(Resource) var park_scene

onready var curtain_anim = $"Chapter Animations"
onready var chapter_text = $"Chapter Title/CenterContainer/Label"
onready var chapter = $Menu

var currentScene
var next_scene_id = "1-1"

# This was a bad idea, but whatever, it doesn't need to be efficient. Lol
onready var Chapters = {
	"Menu": [[""], menu_scene, "1-1"],
	"1-1" : [["You're late for work.", "Don't hit a car or you'll be extra late", "Move with arrow keys or WASD"], driving_scene, "1-2"],
	"1-2" : [["Uhhh, that's not supposed to happen.", "Let's try again, no gravity this time!"], driving_scene2, "1-3"],
	"1-3" : [["I'm sorry, that was my bad again.", "I meant to multiply by X, not X + 1. Try this."], driving_scene3, "1-4"],
	"1-4" : [["Almost there, we just need some challenge.", "Hit the cones to speed up!"], driving_scene4, "1-5"],
	"1-5" : [["Ok, the cone thing was somewhat intentional.", "But I'm not explaining the score issue", "Here we go, one last time!"], driving_scene5, "1-5r"],
	"1-1r": [[], driving_scene, "1-2"],
	"1-2r": [[], driving_scene2, "1-3"],
	"1-3r": [[], driving_scene3, "1-4"],
	"1-4r": [[], driving_scene4, "1-5"],
	"1-5r": [[], driving_scene5, "1-5r"],
	"1-6r": [[], driving_scene6, "1-6r"],
	"zoom": [["You hit 88 miles per hour.", "Your car has traveled back to the future.", "I fixed the cone spawner.", "But not the silly high score.", "You get to keep that one."], driving_scene5, "1-5r"],
	"why": [["Stay inside the level, please."], driving_scene5, "1-5r"],
	"why2": [["Are you a video game tester?", "There are no bugs out there", "Stay inside the level, please."], driving_scene6, "1-6r"],
	"dead": [["You were hit by a truck.", "The truck driver was also trying to cheat.", "I guess no one won that time.", "I fixed the cener lane exploit. Good luck."], driving_scene6, "1-6r"],
}


func _ready():
	randomize()
	Console.add_command("next_chapter", self, '_next_chapter')\
	.set_description("Go to the next chapter or scene")\
	.register()
	Console.add_command("select_scene", self, '_select_scene')\
	.set_description("Go to a particular chapter or scene." +\
		" Valid arguments are 1-1, 1-2, 2-1, 2-2, 3-1, or Menu")\
	.add_argument('scene', TYPE_STRING)\
	.register()


func _next_chapter():
	Print.info("Going to the next chapter!")
	_select_scene(next_scene_id)


func _next_chapter_silent():
	Print.info("Going to the next chapter!")
	_select_scene_silent(next_scene_id)


func play_scene_end_sound():
	$AudioStreamPlayer.play()


func _select_scene(scene):
	if scene != "1-1":
		play_scene_end_sound()
	_select_scene_silent(scene)


func _select_scene_silent(scene):
	Print.info("Going to chapter " + scene)
	_goto_chapter(Chapters[scene][0], Chapters[scene][1])
	next_scene_id = Chapters[scene][2]


func _goto_chapter(text: Array, new_chapter: Resource):
	curtain_anim.play("Chapter Title Fade In")
	yield(curtain_anim, "animation_finished")
	if chapter != null:
		remove_child(chapter)
		chapter.queue_free()
		chapter = null

	# Show the text while loading.
	if text.size() >= 1:
		for title in text:
			chapter_text.text = title
			curtain_anim.play("Text Fade In")
			yield(curtain_anim, "animation_finished")
			curtain_anim.play("Text Fade Out")
			yield(curtain_anim, "animation_finished")

	chapter = new_chapter.instance()
	add_child(chapter)
	chapter.connect("scene_end", self, "_next_chapter")

	curtain_anim.play("Chapter Title Fade Out")
	yield(curtain_anim, "animation_finished")
