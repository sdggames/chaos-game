extends KinematicBody2D

export var speed_factor := 0.5
export var drift := 0.1
export var fix_speed_error := false
var velocity = Vector2.ZERO
var target_velocity
var back_to_the_future := false
var flames := false

signal hit_car
signal hit_cone


func get_input():
	target_velocity = Vector2.ZERO
	if Input.is_action_pressed("right") or back_to_the_future:
		target_velocity += Vector2.RIGHT
	if Input.is_action_pressed("left"):
		target_velocity += Vector2.LEFT
	if Input.is_action_pressed("down"):
		target_velocity += Vector2.DOWN
	if Input.is_action_pressed("up"):
		target_velocity += Vector2.UP
	target_velocity = target_velocity.normalized() * Global.scroll_speed * speed_factor
	if !fix_speed_error:
		velocity = (velocity * (1 - drift) + target_velocity * (1 + drift))


func _process(_delta):
	if !flames and Global.high_score_is_high:
		on_high_score()
	$FlamesD.initial_velocity = Global.scroll_speed
	$FlamesP.initial_velocity = Global.scroll_speed


func _physics_process(delta):
	get_input()
	if fix_speed_error:
		var drift_delta = drift * delta
		velocity = (velocity * (1 - drift_delta) + target_velocity * (drift_delta))
	velocity = move_and_slide(velocity)
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.is_in_group("Car"):
			hit_car()


func on_high_score():
	flames = true
	$Warp.play()
	$FlamesD.emitting = true
	$FlamesP.emitting = true


func back_to_the_fuuuuuture():
	on_high_score()
	$Sprite.visible = false
	$CollisionShape2D.visible = false
	back_to_the_future = true


func _on_Area2D_body_entered(body):
	if body.is_in_group("Cone") and !Global.scene_ending:
		body.global_transform.origin.x -= 10000
		body.pshwaaah() # Make that sound!!!!!
		get_tree().call_group("Camera", "start_shake")
		emit_signal("hit_cone")


func _on_Crunch_body_entered(body):
	if body.is_in_group("Car"):
		hit_car()


func hit_car():
		if (!$AudioStreamPlayer.playing and !Global.scene_ending):
			$AudioStreamPlayer.play()
			emit_signal("hit_car")
