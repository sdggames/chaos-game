extends Node2D

signal reading_timer

export var scene_ready := false
export var player_controlling := false
export var scroll_speed := 500.0
export var driving_finished_narrative = false
export var finally_playable := false
export var cheat_bug_found := false
export var high_score_is_high := false
export var score_drop_speed := 0.25
export var scene_number_is_high_enough_for_us_to_do_the_flashy_thing := false
export var scene_ending := false


var score = 0
var high_score = 0
var score_factor = 1


func wait(time):
	yield(get_tree().create_timer(time), "timeout")
	emit_signal("reading_timer")


func reset_score():
	scene_ending = false
	score = 0
	score_factor = 1
	high_score_is_high = false


#	add points to the scoreboard
func add_score():
	if !scene_ending:
		score += score_factor
		if score > high_score:
			high_score = score
			if scene_number_is_high_enough_for_us_to_do_the_flashy_thing:
				high_score_is_high = true


# Lower the game speed, NOT the player score! Fixed it! Name isn't changing because LAZY
func lower_score(baseline):
	if !high_score_is_high:
		if scroll_speed > baseline:
			scroll_speed -= score_drop_speed

