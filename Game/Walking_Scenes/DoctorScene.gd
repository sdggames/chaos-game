extends Node2D

signal scene_end


func _ready():
	$TextEngine.load_scene(1, 2)
	$TextEngine.start_reading()
	Global.driving_finished_narrative = false


func _on_Scene_end(_d = null):
	emit_signal("scene_end")
