extends Node2D

signal scene_end


func _ready():
	$TextEngine.start_scene(2, 1)


func _on_Scene_end():
	emit_signal("scene_end")


func _on_TextEngine_narrative_event(event):
	if event == "Shopping":
		$TextEngine.start_reading()
