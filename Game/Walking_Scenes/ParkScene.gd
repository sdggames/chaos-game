extends Node2D

signal scene_end

export(Resource) var player_combat

onready var camera = $Player/Camera2D
var invitation = false
var seated = false
var in_combat = false


func _ready():
	var _d = $TextEngine.connect("narrative_event", self, "_narrative_event")
	$TextEngine.load_scene(3, 1)


func _narrative_event(event):
	if event == "Sit":
		Print.info("Asked player to sit down.")
		invitation = true
	elif event == "Combat":
		Print.info("Start player combat.")
		var player: KinematicBody2D = player_combat.instance()
		add_child(player)
		player.position = $Interactable.position
		var _d = player.connect("attack_landed", self, "_attack_landed")
		remove_child(camera)
		player.add_child(camera)
		camera.position = Vector2.ZERO
		in_combat = true
	elif event == "MonsterSlain":
		Print.info("Game Won. Begin Epilogue")
		emit_signal("scene_end")


func _input(event):
	if invitation and !in_combat:
		if event.is_action("act") and $Interactable.get_overlapping_bodies().size() > 0:
			var worldpos = camera.global_position
			Print.info("Asked player to sit down")
			$Player.remove_child(camera)
			add_child(camera)
			camera.position = worldpos
			$Player.queue_free()
			$TextEngine.start_reading()


func _attack_landed():
		Print.info("Player attack")
		$TextEngine.start_reading()


func _on_Bench_Area_body_entered(body):
	if !invitation and body.is_in_group("Player"):
		$TextEngine.start_reading()
		$Monster.state = $Monster.GOTO_TARGET
