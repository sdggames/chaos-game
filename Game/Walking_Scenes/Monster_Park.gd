extends KinematicBody2D

export var move_speed := 150
onready var player = $"../Player"
onready var wait_target = $"../WaitTarget"
var state := FOLLOW_PLAYER

enum {
	FOLLOW_PLAYER,
	GOTO_TARGET,
}


func _physics_process(_delta):
	var move_direction
	if state == FOLLOW_PLAYER:
		move_direction = (player.position - position).normalized()
	else:
		move_direction = (wait_target.position - position).normalized()
	var _d = move_and_slide(move_direction * move_speed)
