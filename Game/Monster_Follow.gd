extends Node2D

export var move_speed = 100
onready var player = get_node("../Player")


func _physics_process(delta):
	if visible:
		if player.transform.origin.y < transform.origin.y:
			transform.origin += Vector2.UP * move_speed * delta
		elif abs(player.transform.origin.y - transform.origin.y) > move_speed * delta:
			transform.origin += Vector2.DOWN * move_speed * delta
		else:
			transform.origin = Vector2(transform.origin.x, player.transform.origin.y)
