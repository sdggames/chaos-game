# Chaos Game for Brackeys jam
### This project is built on top of my base game template.
This project uses git LFS as well as git subrepositories. To work in this repo, use the following commands:

    git lfs install
    git clone https://gitlab.com/sdggames/godot-common-library.git FOLDER
    cd FOLDER
    git submodule update --init
<br/><br/>

## Tools I made
### Automatic CICD with automatic deployment to itch.io and version tracking.
- All commits to development branches will build for common platforms and generate a HTML5 playable game in-browser, along with downloadable artifacts.
- Commits to the release branch will build a release version and publish it to itch.io
- Release builds can also be triggered and sent to a separate branch on itch.io
- TODO: unit tests will also automatically run on dev branches and master.

### FPS Menu
- A FPS display can be activated via menu or the command terminal plugin.
- Fps display will show the min, max, and average frame counts
<br/><br/>

## Tools other people made
### Command Terminal
- [Console by quentincaffeino](https://github.com/quentincaffeino/godot-console) is included in the addons folder.

### Print Singleton
- [Logging script by spooner](https://gist.github.com/Spooner/0daff3fd31411488fe1b) included in the addons folder.
<br/><br/>

## Credits/Licenses/Acknowledgments
CICD solution was inspired by [barichello's example](https://gitlab.com/barichello/godot-ci)
