include: VersionInfo.yml

stages:
  - build_dependencies
  - build
  - test
  - pack
  - deploy

variables:
  REVISION_NUMBER: ${RELEASE_NUMBER}_${CI_COMMIT_SHORT_SHA}
  EXPORT_NAME_RELEASE: ${GAME_NAME}_${RELEASE_NUMBER}
  EXPORT_NAME_INTERNAL: ${GAME_NAME}_${RELEASE_NUMBER}_${CI_COMMIT_SHORT_SHA}
  GIT_SUBMODULE_STRATEGY: recursive

# ================================================================================================================
#                                                   Job Dependencies
# ================================================================================================================
# =+=+=+=+=+=+=+= BUILD COMMON =+=+=+=+=+=+=+=
.build-internal: &build-internal
  image: barichello/godot-ci:$GODOT_VERSION
  script:
    - mkdir -v -p build/$BUILD_FOLDER
    - cd $GAME_FOLDER/
    - godot -v $BUILD_FLAGS --export "$BUILD_TARGET" ../build/$BUILD_FOLDER/$EXPORT_NAME_INTERNAL$BUILD_FILE_EXTENSION
  artifacts:
    name: $EXPORT_NAME_INTERNAL
    paths:
      - build/
    expire_in: 6 hour # We have a consolidated copy created in the final stage, just keep this while the pipeline might be running.

.build-web-internal: &build-web-internal
  image: barichello/godot-ci:$GODOT_VERSION
  script:
    - mkdir -v -p build/$BUILD_FOLDER
    - cd $GAME_FOLDER/
    - godot -v $BUILD_FLAGS --export "$BUILD_TARGET" ../build/$BUILD_FOLDER/index.html
  artifacts:
    name: $EXPORT_NAME_INTERNAL
    paths:
      - build/
    expire_in: 6 hour # We have a consolidated copy created in the final stage, just keep this while the pipeline might be running.

.build-release: &build-release
  image: barichello/godot-ci:$GODOT_VERSION
  script:
    - mkdir -v -p build/$BUILD_FOLDER
    - cd $GAME_FOLDER/
    - godot -v $BUILD_FLAGS --export "$BUILD_TARGET" ../build/$BUILD_FOLDER/$EXPORT_NAME_RELEASE$BUILD_FILE_EXTENSION
  artifacts:
    name: $EXPORT_NAME_RELEASE
    paths:
      - build/
    expire_in: 6 hour # We have a consolidated copy created in the final stage, just keep this while the pipeline might be running.

.build-web-release: &build-web-release
  image: barichello/godot-ci:$GODOT_VERSION
  script:
    - mkdir -v -p build/$BUILD_FOLDER
    - cd $GAME_FOLDER/
    - godot -v $BUILD_FLAGS --export "$BUILD_TARGET" ../build/$BUILD_FOLDER/index.html
  artifacts:
    name: $EXPORT_NAME_RELEASE
    paths:
      - build/
    expire_in: 6 hour # We have a consolidated copy created in the final stage, just keep this while the pipeline might be running.

# =+=+=+=+=+=+=+= BUILD SETTINGS =+=+=+=+=+=+=+=
.linux_release: &linux_release
  variables:
    BUILD_FILE_EXTENSION: .x86_64
    BUILD_TARGET: "Linux/X11"
    BUILD_FOLDER: "linux-release"
    BUILD_FLAGS: ""

.linux_debug: &linux_debug
  variables:
    BUILD_FILE_EXTENSION: .x86_64
    BUILD_TARGET: "Linux/X11"
    BUILD_FOLDER: "linux-debug"
    BUILD_FLAGS: "-d"

.windows_release: &windows_release
  variables:
    BUILD_FILE_EXTENSION: .exe
    BUILD_TARGET: "Windows Desktop"
    BUILD_FOLDER: "windows-release"
    BUILD_FLAGS: ""

.windows_debug: &windows_debug
  variables:
    BUILD_FILE_EXTENSION: .exe
    BUILD_TARGET: "Windows Desktop"
    BUILD_FOLDER: "windows-debug"
    BUILD_FLAGS: "-d"

.mac_release: &mac_release
  variables:
    BUILD_FILE_EXTENSION: .zip
    BUILD_TARGET: "Mac OSX"
    BUILD_FOLDER: "mac-release"
    BUILD_FLAGS: ""

.mac_debug: &mac_debug
  variables:
    BUILD_FILE_EXTENSION: .zip
    BUILD_TARGET: "Mac OSX"
    BUILD_FOLDER: "mac-debug"
    BUILD_FLAGS: "-d"

.web_release: &web_release
  variables:
    BUILD_FILE_EXTENSION: index.html
    BUILD_TARGET: "HTML5"
    BUILD_FOLDER: "web-release"
    BUILD_FLAGS: ""

.web_debug: &web_debug
  variables:
    BUILD_FILE_EXTENSION: index.html
    BUILD_TARGET: "HTML5"
    BUILD_FOLDER: "web-debug"
    BUILD_FLAGS: "-d"

# =+=+=+=+=+=+=+= DEPLOY COMMON =+=+=+=+=+=+=+=
.itch_deploy: &itch_deploy
  image: barichello/godot-ci:$GODOT_VERSION
  stage: deploy
  script:
    - butler push ./build/linux-release $GROUP_NAME/$GAME_NAME:linux$STREAM_SUFFIX
    - butler push ./build/windows-release $GROUP_NAME/$GAME_NAME:windows$STREAM_SUFFIX
    - butler push ./build/mac-release $GROUP_NAME/$GAME_NAME:mac$STREAM_SUFFIX
    - butler push ./build/web-release $GROUP_NAME/$GAME_NAME:web$STREAM_SUFFIX

# =+=+=+=+=+=+=+= DEPLOY SETTINGS =+=+=+=+=+=+=+=
.needs_release: &needs_release
  needs:
    - build_release:linux-release
    - build_release:windows-release
    - build_release:mac-release
    - build_release:web-release

.needs_internal: &needs_internal
  needs:
    - build_internal:linux-release
    - build_internal:windows-release
    - build_internal:mac-release
    - build_internal:web-release

# ================================================================================================================
#                                                           Jobs
# ================================================================================================================
# =+=+=+=+=+=+=+= BUILD JOBS - INTERNAL =+=+=+=+=+=+=+=
build_internal:windows-debug:
  stage: build
  <<: *windows_debug
  <<: *build-internal
  except:
    - /^release.*/

build_internal:windows-release:
  stage: build
  <<: *windows_release
  <<: *build-internal
  except:
    - /^release.*/

build_internal:linux-debug:
  stage: build
  <<: *linux_debug
  <<: *build-internal
  except:
    - /^release.*/

build_internal:linux-release:
  stage: build
  <<: *linux_release
  <<: *build-internal
  only:
    - master

build_internal:mac-debug:
  stage: build
  <<: *mac_release
  <<: *build-internal
  except:
    - /^release.*/

build_internal:mac-release:
  stage: build
  <<: *mac_release
  <<: *build-internal
  only:
    - master

build_internal:web-debug:
  stage: build
  <<: *web_debug
  <<: *build-web-internal
  except:
    - /^release.*/

build_internal:web-release:
  stage: build
  <<: *web_release
  <<: *build-web-internal
  except:
    - /^release.*/

# =+=+=+=+=+=+=+= BUILD JOBS - RELEASE =+=+=+=+=+=+=+=
build_release:linux-release:
  stage: build
  <<: *linux_release
  <<: *build-release
  only:
  - /^release.*/

build_release:windows-release:
  stage: build
  <<: *windows_release
  <<: *build-release
  only:
  - /^release.*/

build_release:mac-release:
  stage: build
  <<: *mac_release
  <<: *build-release
  only:
  - /^release.*/

build_release:web-release:
  stage: build
  <<: *web_release
  <<: *build-web-release
  only:
  - /^release.*/

# =+=+=+=+=+=+=+= PACK JOBS =+=+=+=+=+=+=+=
# Pack the Windows Zip Redistributable
pack:WindowsPortable:
  image: sdggames/zip:latest
  stage: pack
  needs: ["build_internal:windows-debug", "build_internal:windows-release"]
  script:
    - cd ./build/windows-release/
    - zip -r "../$Windows_EXPORT_NAME_INTERNAL.zip" .
    - cd ../windows-debug
    - zip -r "../$Windows_debug_EXPORT_NAME_INTERNAL.zip" .
  artifacts:
    paths:
      - ./build/
    expire_in: 6 hour # We have a consolidated copy created in the final stage, just keep this while the pipeline might be running.
  except: 
    - /^release.*/

# GitLab Pages Deploy
deployWeb:
  image: alpine:latest
  stage: pack
  needs: ["build_internal:web-debug", "build_internal:web-release"]
  script:
    - mkdir -v -p public/debug
    - mkdir -v -p public/release
    - mv build/web-debug/** ./public/debug
    - mv build/web-release/** ./public/release
  artifacts:
    paths:
      - public
  except: 
    - /^release.*/

# Contain all of the builds in a single artifact so we don't have to download all the things!
consolidateArtifacts:
  image: alpine:latest
  stage: pack
  script: "ls ./build"
  artifacts:
    name: $GAME_NAME_$REVISION_NUMBER
    expose_as: "Build Output"
    paths:
      - ./build/
    expire_in: 1 day # This is huge, we probably want to save it to a repo somewhere so we don't run out of space.
  except: 
    - /^release.*/

# =+=+=+=+=+=+=+= DEPLOY JOBS =+=+=+=+=+=+=+=
itchio-unstable:
  <<: *needs_internal
  <<: *itch_deploy
  variables:
    STREAM_SUFFIX: ${UNSTABLE_STREAM_SUFFIX}
  only:
    - master
  when: manual
    
itchio-beta:
  <<: *needs_release
  <<: *itch_deploy
  variables:
    STREAM_SUFFIX: ${BETA_STREAM_SUFFIX}
  only:
    - /^release.*/
  when: manual

itchio-release:
  <<: *needs_release
  <<: *itch_deploy
  variables:
    STREAM_SUFFIX: ${RELEASE_STREAM_SUFFIX}
  only:
    - /^release.*/
  when: manual